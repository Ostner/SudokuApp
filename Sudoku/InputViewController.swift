//
//  InputViewController.swift
//  Sudoku
//
//  Created by Tobias Ostner on 7/14/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

protocol InputViewControllerDelegate: class {
    func setNumber(_ number: String, at index: Int)
}

class InputViewController: UIViewController, UIViewControllerTransitioningDelegate {

    let lineWidth: CGFloat = 16

    let path = UIBezierPath()

    var isCompound = false

    var timer: Timer?

    var cellIndex: Int!

    weak var delegate: InputViewControllerDelegate?

    @IBOutlet weak var canvas: CanvasView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let pan = UIPanGestureRecognizer(target: self, action: #selector(draw))
        canvas.addGestureRecognizer(pan)
        canvas.lineWidth = lineWidth
    }

    override var transitioningDelegate: UIViewControllerTransitioningDelegate? {
        get {
            return self
        }
        set {
            return
        }
    }

    @objc func draw(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            if !isCompound {
                path.removeAllPoints()
                isCompound = true
            }
            path.move(to:  sender.location(in: canvas))
            canvas.path = path
            timer?.invalidate()
        case .changed:
            path.addLine(to: sender.location(in: canvas))
            canvas.path = path
        default:
            timer = Timer.scheduledTimer(
                withTimeInterval: 1.5,
                repeats: false) { [weak self] _ in
                    DispatchQueue.main.async {
                        self?.isCompound = false
                        self?.makePrediction()
                    }
            }
        }
    }

    func makePrediction() {
        let number = predictor.predict(pathImage)
        delegate?.setNumber("\(number)", at: cellIndex)
    }


    // MARK: UIViewControllerTransitioningDelegate

    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController)
        -> UIPresentationController?
    {
        return InputPresentationController(
            presentedViewController: presented,
            presenting: presenting)
    }

    // MARK: Private

    private var pathImage: UIImage {
        let x = path.bounds.origin.x - lineWidth
        let y = path.bounds.origin.y - lineWidth
        let height = path.bounds.height + 2*lineWidth
        let width = path.bounds.width + 2*lineWidth
        let rect = CGRect(x: x, y: y, width: width, height: height)
        let renderer = UIGraphicsImageRenderer(bounds: rect)
        return renderer.image { _ in path.stroke() }
    }

    private let predictor = DigitPredictor()

}
