//
//  DigitPredictor.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 6/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreML

class DigitPredictor {

    let dim = NSNumber(integerLiteral: 784)

    func predict(_ image: UIImage) -> Int {
        let preprocessor = DigitPreprocessor(image: image)
        preprocessor.run()
        let input = preprocessor.processed
        let m = try! MLMultiArray(shape: [dim], dataType: .double)
        for (index, elem) in input.enumerated() {
            m[index] = NSNumber(value: elem)
        }
        let clf = NNClassifier()
        let prediction = try! clf.prediction(image: m)
        return prediction.probabilities.argmax!
    }
}
