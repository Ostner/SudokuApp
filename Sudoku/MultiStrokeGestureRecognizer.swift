//
//  MultiStrokeGestureRecognizer.swift
//  Sudoku
//
//  Created by Tobias Ostner on 7/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit.UIGestureRecognizerSubclass
import UIKit

class MultiStrokeGestureRecognizer: UIGestureRecognizer {
    
    @IBInspectable var delay: Double = 1.5

    private(set) var allPoints: [[CGPoint]] = []

    private var timer: Timer?
    private var currentPoints: [CGPoint] = []
    private var touch: UITouch?

    override func awakeFromNib() {
        currentPoints = []
        allPoints = []
        delay = 1.5
    }

    var isNewStroke: Bool {
        return currentPoints.count <= 1
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        guard let touch = touches.first, touches.count == 1 else { state = .failed; return }
        self.touch = touch
        currentPoints = []
        currentPoints.append(touch.location(in: view))
        state = allPoints.isEmpty ? .began : .changed
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        guard let touch = touches.first, self.touch == touch else { state = .failed; return }
        currentPoints.append(touch.location(in: view))
        state = .changed
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        state = .cancelled
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        guard let touch = touches.first,
              self.touch == touch,
              currentPoints.count > 1
            else { print("failed"); state = .failed; return }
        allPoints.append(currentPoints)
        if isTimerValid {
            scheduleNewTimer()
            state = .changed
        } else {
            state = .ended
        }
    }

    override func reset() {
        super.reset()
        timer?.invalidate()
        timer = nil
        allPoints = []
        currentPoints = []
        touch = nil
    }

    override func location(in view: UIView?) -> CGPoint {
        guard let lastPoint = currentPoints.last else { fatalError("No points accumulated yet")}
        return lastPoint
    }

    private func scheduleNewTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(
            withTimeInterval: delay,
            repeats: false) { [weak self] _ in
                self?.state = .ended
        }
    }

    private var isTimerValid: Bool {
        guard let timer = timer else { return true }
        return timer.isValid
    }

}
