//
//  SudokuView.swift
//  Sudoku
//
//  Created by Tobias Ostner on 6/21/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

@IBDesignable class SudokuView: UIView {

    @IBInspectable var fontSize: CGFloat = 65 {
        didSet {
            cells.forEach {
                $0.font = UIFont.boldSystemFont(ofSize: fontSize)
            }
        }
    }

    @IBInspectable var cellBackgroundColor: UIColor? {
        didSet {
            refreshCellColors()
        }
    }

    @IBInspectable var cellTextColor: UIColor? {
        didSet {
            refreshCellColors()
        }
    }

    @IBInspectable var presetTextColor: UIColor? {
        didSet {
            refreshCellColors()
        }
    }

    @IBInspectable var normalGridWidth: CGFloat = 5 {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable var boxGridWidth: CGFloat = 10 {
        didSet {
            setNeedsDisplay()
        }
    }

    var gridColor: UIColor? {
        didSet {
            backgroundColor = gridColor
        }
    }

    var presetCells: Set<Int> = [] {
        didSet {
            refreshCellColors()
        }
    }

    func setNumber(number: String, at index: Int, isPreset: Bool = false) {
        cells[index].text = number
        if isPreset {
            cells[index].backgroundColor = presetBackgroundColor
            presetCells.insert(index)
        }
    }

    var cellWidth: CGFloat {
        return (bounds.width - 6*normalGridWidth - 4*boxGridWidth) / 9
    }

    var cellHeight: CGFloat {
        return (bounds.height - 6*normalGridWidth - 4*boxGridWidth) / 9
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        cells.forEach { addSubview($0) }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        cells.forEach { addSubview($0) }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        var x: CGFloat = 0
        var y: CGFloat = 0
        for (index, cell) in cells.enumerated() {
            let deltaGridWidth = boxGridWidth - normalGridWidth
            x = CGFloat(index % 9) * cellWidth
            x += CGFloat(index % 9) * normalGridWidth + normalGridWidth
            x += CGFloat((index % 9) / 3) * deltaGridWidth + deltaGridWidth
            y = CGFloat(index / 9) * cellHeight
            y += CGFloat(index / 9) * normalGridWidth + normalGridWidth
            y += CGFloat(index / 27) * deltaGridWidth + deltaGridWidth
            cell.frame = CGRect(x: x, y: y, width: cellWidth, height: cellHeight)
        }
    }

    // MARK: IB

    override func prepareForInterfaceBuilder() {
        let sudoku = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
        for (index, char) in sudoku.enumerated() {
            if let number = Int(String(char)) {
                setNumber(number: "\(number)", at: index, isPreset: true)
            }
        }
        setNumber(number: "2"   , at: 1)
    }

    // MARK: Private

    private(set) lazy var cells: [UILabel] = {
        var result: [UILabel] = []
        for index in 0 ..< 81 {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.backgroundColor = self.cellBackgroundColor
            label.font = UIFont.systemFont(ofSize: self.fontSize)
            label.textAlignment = .center
            label.isUserInteractionEnabled = true
            result.append(label)
        }
        return result
    }()

    private func refreshCellColors() {
        presetBackgroundColor = makeBackgroundPattern()
        for (index, cell) in cells.enumerated() {
            cell.textColor = presetCells.contains(index) ? presetTextColor : cellTextColor
            cell.backgroundColor = presetCells.contains(index) ? presetBackgroundColor : cellBackgroundColor
        }
    }

    private var presetBackgroundColor: UIColor?

    private func makeBackgroundPattern() -> UIColor? {
        guard let background = cellBackgroundColor,
              let foreground = cellTextColor
        else { return nil }
        let size = CGSize(width: 6, height: 6)
        let renderer = UIGraphicsImageRenderer(size: size)
        let points = renderer.image {
            context in
            let imageContext = context.cgContext
            imageContext.setFillColor(background.cgColor)
            imageContext.fill(CGRect(origin: .zero, size: size))
            imageContext.setFillColor(foreground.cgColor)
            imageContext.addEllipse(in: CGRect(x: 1.5, y: 1.5, width: 3, height: 3))
            imageContext.fillPath()
        }
        return UIColor(patternImage: points)
    }

}
