//
//  CanvasView.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 6/25/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class CanvasView: UIView {
    var path: UIBezierPath? {
        didSet {
            setNeedsDisplay()
        }
    }

    var lineWidth: CGFloat = 16

    override func draw(_ rect: CGRect) {
        guard let path = path else { return }
        path.lineWidth = lineWidth
        path.lineCapStyle = .round
        path.lineJoinStyle = .round
        UIColor.black.setStroke()
        path.stroke()
    }
}
