//
//  Sudoku.swift
//  Sudoku
//
//  Created by Tobias Ostner on 6/22/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

struct Sudoku {

    enum difficulty: String {
        case light, easy, difficult, impossible
    }

    let cellCount = 81

    let digits = 1 ... 9

    let rowUnits =
        [[ 0,  1,  2,  3,  4,  5,  6,  7,  8],
         [ 9, 10, 11, 12, 13, 14, 15, 16, 17],
         [18, 19, 20, 21, 22, 23, 24, 25, 26],
         [27, 28, 29, 30, 31, 32, 33, 34, 35],
         [36, 37, 38, 39, 40, 41, 42, 43, 44],
         [45, 46, 47, 48, 49, 50, 51, 52, 53],
         [54, 55, 56, 57, 58, 59, 60, 61, 62],
         [63, 64, 65, 66, 67, 68, 69, 70, 71],
         [72, 73, 74, 75, 76, 77, 78, 79, 80]]

    let columnUnits =
        [[0,  9, 18, 27, 36, 45, 54, 63, 72],
         [1, 10, 19, 28, 37, 46, 55, 64, 73],
         [2, 11, 20, 29, 38, 47, 56, 65, 74],
         [3, 12, 21, 30, 39, 48, 57, 66, 75],
         [4, 13, 22, 31, 40, 49, 58, 67, 76],
         [5, 14, 23, 32, 41, 50, 59, 68, 77],
         [6, 15, 24, 33, 42, 51, 60, 69, 78],
         [7, 16, 25, 34, 43, 52, 61, 70, 79],
         [8, 17, 26, 35, 44, 53, 62, 71, 80]]

    let boxUnits =
        [[ 0,  1,  2,  9, 10, 11, 18, 19, 20],
         [ 3,  4,  5, 12, 13, 14, 21, 22, 23],
         [ 6,  7,  8, 15, 16, 17, 24, 25, 26],
         [27, 28, 29, 36, 37, 38, 45, 46, 47],
         [30, 31, 32, 39, 40, 41, 48, 49, 50],
         [33, 34, 35, 42, 43, 44, 51, 52, 53],
         [54, 55, 56, 63, 64, 65, 72, 73, 74],
         [57, 58, 59, 66, 67, 68, 75, 76, 77],
         [60, 61, 62, 69, 70, 71, 78, 79, 80]]

    lazy var allUnits: [[Int]] = {
        return self.rowUnits + self.columnUnits + self.boxUnits
    }()

    lazy var neighbors: [Int: [Int]] = {
        var result: [Int: [Int]] = [:]
        for cell in 0 ..< self.cellCount {
            let others = Set(self.allUnits
                .filter { $0.contains(cell) }
                .flatMap { $0 })
                .symmetricDifference([cell])
            result[cell] = Array(others)
        }
        return result
    }()

    private var cells: [Set<Int>]

    private(set) var presetCells: Set<Int> = []

    subscript(index: Int) -> Int {
        if presetCells.contains(index) {
            return cells[index].first!
        }
        return 0
    }

    init() {
        let empty = Set(digits)
        cells = Array(repeating: empty, count: cellCount)
    }

    init(string: String) {
        self.init()
        let values = string.map { Int(String($0))! }
        for (cell, value) in values.enumerated() where value != 0 {
            preSetValue(value, in: cell)
        }
    }

    mutating func solve() {
        _ = search()
        precondition(isComplete, "Sudoku wasn't solvable")
    }

    static func generateSudoku(fillCount: Int = 17) -> Sudoku {
        var sudoku = Sudoku()
        let shuffled = Array(0 ..< 81).shuffled()
        _generateSudoku(&sudoku, shuffled, fillCount)
        return sudoku
    }

    private static func _generateSudoku(_ sudoku: inout Sudoku, _ pending: [Int], _ fillCount: Int, _ filled: Int = 0) {
        guard fillCount > filled else { return }
        let index = pending[filled]
        for digit in Array(sudoku.cells[index]).shuffled() {
            let old = sudoku
            sudoku.preSetValue(digit, in: index)
            if sudoku.makeConsistent(neighborsOf: index) {
                _generateSudoku(&sudoku, pending, fillCount, filled+1)
                return
            }
            sudoku = old
        }
    }

    private mutating func search() -> Bool {
        guard !isComplete else { return true }
        let next = cells.index { $0.count > 1 }!
        for digit in cells[next] {
            let old = cells
            setValue(digit, in: next)
            if makeConsistent(neighborsOf: next) && search() {
                return true
            } else {
                cells = old
            }
        }
        return false
    }

    private var isComplete: Bool {
        for cell in cells {
            if cell.count > 1 { return false }
        }
        return true
    }

    private mutating func makeConsistent(neighborsOf cell: Int) -> Bool {
        guard cleanUp(neighborsOf: cell) && unique() else { return false }
        return true
    }

    private mutating func cleanUp(neighborsOf cell: Int) -> Bool {
        let value = cells[cell].first!
        for neighbor in neighbors[cell]! where cells[neighbor].count > 1 {
            cells[neighbor].remove(value)
            guard cells[neighbor].count > 0 else { return false }
            if cells[neighbor].count == 1 && !cleanUp(neighborsOf:neighbor) {
                return false
            }
        }
        return true
    }

    private mutating func unique() -> Bool {
        var revised: Bool
        repeat {
            revised = false
            for unit in allUnits {
                for digit in digits {
                    var occurences = (count: 0, index: -1)
                    for cellIndex in unit where cells[cellIndex].count > 1 {
                        if cells[cellIndex].contains(digit) {
                            occurences = (occurences.count+1, cellIndex)
                        }
                    }
                    if occurences.count == 1 {
                        setValue(digit, in: occurences.index)
                        revised = true
                        if !cleanUp(neighborsOf: occurences.index) { return false }
                    }
                }
            }
        } while revised
        return true
    }

    private mutating func setValue(_ value: Int, in cell: Int) {
        guard cells[cell].contains(value) else { fatalError("Value isn't in domain of cell") }
        cells[cell] = [value]
    }

    private mutating func preSetValue(_ value: Int, in cell: Int) {
        setValue(value, in: cell)
        presetCells.insert(cell)
    }

}

extension Sudoku: CustomStringConvertible {
    var description: String {
        var result = ""
        for (index, cell) in cells.enumerated() {
            var digit: String
            if presetCells.contains(index) {
                digit = String(format: " %3d ", cell.first!)
            } else {
                digit = "  .  "
            }
            if (index+1) % 9 == 0 {
                digit += "\n"
            }
            result += digit
        }
        return result
    }
}
