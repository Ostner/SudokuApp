//
//  SudokuViewController.swift
//  Sudoku
//
//  Created by Tobias Ostner on 6/21/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class SudokuViewController: UIViewController, InputViewControllerDelegate {

    var difficulty: Sudoku.difficulty?

    var sudoku: Sudoku!

    @IBOutlet weak var sudokuView: SudokuView!
    @IBOutlet weak var canvas: CanvasView!
    @IBOutlet weak var multiStrokeGesture: MultiStrokeGestureRecognizer!

    override func viewDidLoad() {
        sudoku = generateSudoku()
        setupViews()
    }

    @IBAction func newSudoku() {
        dismiss(animated: true)
    }

    // MARK: Canvas View

    private let lineWidth: CGFloat = 12

    private let path = UIBezierPath()

    @IBAction func draw(sender: MultiStrokeGestureRecognizer) {
        switch sender.state {
        case .possible, .cancelled, .failed:
            return
        case .began:
            canvas.isHidden = false
            path.move(to: sender.location(in: canvas))
            canvas.path = path
        case .changed:
            if sender.isNewStroke {
                path.move(to: sender.location(in: canvas))
            } else {
                path.addLine(to: sender.location(in: canvas))
            }
            canvas.path = path
        case .ended:
            canvas.isHidden = true
            let num = self.predictor.predict(pathImage)
            if let cellIndex = determineCellWithPathBounds() {
                sudokuView.setNumber(number: "\(num)", at: cellIndex)
            }
            path.removeAllPoints()
        }
    }

    // MARK: InputViewControllerDelegate

    func setNumber(_ number: String, at index: Int) {
        sudokuView.setNumber(number: number, at: index)
    }

    // MARK: Private

    private let predictor = DigitPredictor()

    private func setupViews() {
        setAllPresetSudokuCells()
        setupGestureRecognizer()
    }

    private func generateSudoku() -> Sudoku {
        let fillCount: Int
        if let difficulty = difficulty {
            switch difficulty {
            case .light: fillCount = 50
            case .easy: fillCount = 35
            case .difficult: fillCount = 27
            case .impossible: fillCount = 20
            }
        } else {
            fillCount = 50
        }
        return Sudoku.generateSudoku(fillCount: fillCount)
    }

    private func setAllPresetSudokuCells() {
        for index in sudoku.presetCells {
            sudokuView.setNumber(
                number: "\(sudoku[index])",
                at: index,
                isPreset: true)
        }
    }

    private func setupGestureRecognizer() {
        for index in 0..<sudoku.cellCount where !sudoku.presetCells.contains(index) {
            let tap = UITapGestureRecognizer(target: self, action: #selector(cellTapped))
            tap.delegate = self
            sudokuView.cells[index].addGestureRecognizer(tap)
        }
    }

    private var pathImage: UIImage {
        let x = path.bounds.origin.x - lineWidth
        let y = path.bounds.origin.y - lineWidth
        let height = path.bounds.height + 2*lineWidth
        let width = path.bounds.width + 2*lineWidth
        let rect = CGRect(x: x, y: y, width: width, height: height)
        let renderer = UIGraphicsImageRenderer(bounds: rect)
        return renderer.image { _ in path.stroke() }
    }

    private func determineCellWithPathBounds() -> Int? {
        let notPreset = Set(0..<81).subtracting(sudoku.presetCells)
        let indicesSortByArea = notPreset.sorted { fst, snd in
            return areaWithPath(fst) > areaWithPath(snd)
        }
        let max = indicesSortByArea.first!
        return max
    }

    private func areaWithPath(_ idx: Int) -> CGFloat {
        let rect = sudokuView.convert(sudokuView.cells[idx].frame, to: canvas)
        let intersection = rect.intersection(path.bounds)
        return intersection.width * intersection.height
    }

    @objc private func cellTapped(sender: UITapGestureRecognizer) {
        let inputVC = storyboard!.instantiateViewController(withIdentifier: "InputViewController") as! InputViewController
        inputVC.modalPresentationStyle = .custom
        inputVC.cellIndex = sudokuView.cells.index(of: sender.view as! UILabel)
        inputVC.delegate = self
        present(inputVC, animated: false)
    }
}

extension SudokuViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer == multiStrokeGesture ? true : false
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer == multiStrokeGesture ? true : false
    }
}
