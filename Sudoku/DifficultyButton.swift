//
//  DifficultyButton.swift
//  Sudoku
//
//  Created by Tobias Ostner on 7/10/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

@IBDesignable class DifficultyButton: UIView {

    @IBInspectable var text: String = "" {
        didSet {
            label.text = text
        }
    }

    @IBInspectable var textColor: UIColor? {
        didSet {
            label.textColor = textColor
        }
    }

    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.textAlignment = .center
        label.text = "test"
        label.font = UIFont.preferredFont(forTextStyle: .title1, compatibleWith: nil)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor),
            label.trailingAnchor.constraint(equalTo: trailingAnchor),
            label.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
        addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor),
            label.trailingAnchor.constraint(equalTo: trailingAnchor),
            label.bottomAnchor.constraint(equalTo: bottomAnchor),
            label.topAnchor.constraint(equalTo: topAnchor)
            ])

    }

    override func layoutSubviews() {
        bounds.size.height = bounds.width
        layer.cornerRadius = bounds.width / 2
        super.layoutSubviews()
    }

}
