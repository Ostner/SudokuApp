//
//  NewButton.swift
//  Sudoku
//
//  Created by Tobias Ostner on 8/11/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class NewButton: UIButton {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if type(of: gestureRecognizer) == MultiStrokeGestureRecognizer.self {
            return false
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
}
