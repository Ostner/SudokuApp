//
//  Extensions.swift
//  Sudoku
//
//  Created by Tobias Ostner on 7/10/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreML

extension Array {
    func shuffled() -> [Element]{
        var result = self
        for index in Swift.stride(from: count-1, to: 0, by: -1) {
            let other = Int(arc4random_uniform(UInt32(index+1)))
            result.swapAt(index, other)
        }
        return result
    }
}

extension UIImage {
    var alphaValues: [UInt8]? {
        let dataSize = Int(size.width * size.height)
        var pixelData = [UInt8](repeating: 0, count: dataSize)
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let context = CGContext(
            data: &pixelData,
            width: Int(size.width),
            height: Int(size.height),
            bitsPerComponent: 8,
            bytesPerRow: Int(size.width),
            space: colorSpace,
            bitmapInfo: CGImageAlphaInfo.alphaOnly.rawValue)
        context?.draw(cgImage!, in: CGRect(origin: .zero, size: size))
        return pixelData
    }
}

extension MLMultiArray {
    var argmax: Int? {
        guard count > 0 else { return nil }
        var arg = 0
        for index in 1 ..< count {
            if self[arg].compare(self[index]) == .orderedAscending {
                arg = index
            }
        }
        return arg
    }
}

